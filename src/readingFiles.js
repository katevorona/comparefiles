'use strict';

const fs = require('fs');
const os = require('os');

/**
 * @param {String} filePath
 * @return {Array|null}
 */
function convertFileToArr(filePath) {
    try {
        fs.accessSync(filePath, fs.R_OK);
        const fileText = fs.readFileSync(filePath).toString();
        return fileText.split(os.EOL);
    } catch(e){
        console.error(`File "${filePath}" doesn't exist or you don't have access for reading it \n `);
        return null;
    }
}

/**
 * @param {Array} filesPath
 * @return {String} resultArr.filename
 * @return {Array[]} resultArr.fileArr
 */
function getArrFromFiles(filesPath) {
    let resultArr = [];
    filesPath.forEach((filePath) => {
        let arrFromFile = convertFileToArr(filePath);
        if(arrFromFile !== null) {
            resultArr.push({
                filename: filePath,
                fileArr: arrFromFile
            });
        }
    });
    return resultArr;
}

module.exports = {
    getArrFromFiles: getArrFromFiles
};
