'use strict';

const STATUS_DELETED = '-';
const STATUS_ADDED = '+';
const STATUS_MODIFIED = '*';
const STATUS_NOT_CHANGED = ' ';

/**
 * @param {String|undefined} line1
 * @param {String|undefined} line2
 * @returns {Object}
 */
function getLineInfo(line1, line2) {
    if (typeof line1 === "undefined") {
        return {
            status: STATUS_ADDED,
            value: line2
        };
    }

    if (typeof line2 === "undefined") {
        return {
            status: STATUS_DELETED,
            value: line1
        };
    }

    return {
        status: STATUS_MODIFIED,
        value: `${line1}|${line2}`
    };
}

module.exports = {
    statuses: {
        STATUS_DELETED: STATUS_DELETED,
        STATUS_ADDED: STATUS_ADDED,
        STATUS_MODIFIED: STATUS_MODIFIED,
        STATUS_NOT_CHANGED: STATUS_NOT_CHANGED
    },
    getLineInfo: getLineInfo
};