'use strict';

const lineInfo = require('./lineInfo');
const STATUSES = lineInfo.statuses;

/**
 * @param {Array} file1Arr
 * @param {Array} file2Arr
 * @returns {Array}
 */
function compareFiles(file1Arr, file2Arr) {
    let result = [];

    let matchIndex1 = 0;
    let matchIndex2 = 0;

    let matchPreviousIndex1 = 0;
    let matchPreviousIndexStart = 0;

    const filesMaxLength = Math.max(file1Arr.length, file2Arr.length);

    for (let i = 0; i <= filesMaxLength; i++) {

        matchIndex1 = 0;

        for (let j = matchIndex2; j < file2Arr.length; j++) {
            if (file1Arr[i] === file2Arr[j]) {
                matchIndex1 = i;
                matchIndex2 = j;
                break;
            }
        }

        const matchNotFound = typeof file1Arr[i] === "undefined" || typeof file2Arr[i] === "undefined";

        if (matchNotFound) {
            matchIndex1 = file1Arr.length;
            matchIndex2 = file2Arr.length;
        }

        if (matchIndex1) {

            let linesBeforeMatch1 = file1Arr.slice(matchPreviousIndex1, matchIndex1);
            let linesBeforeMatch2 = file2Arr.slice(matchPreviousIndexStart, matchIndex2);

            const maxLength = Math.max(linesBeforeMatch1.length, linesBeforeMatch2.length);

            for (let i = 0; i < maxLength; i++) {
                result.push(lineInfo.getLineInfo(linesBeforeMatch1[i], linesBeforeMatch2[i]));
            }

            if(!matchNotFound) {
                result.push({
                    status: STATUSES.STATUS_NOT_CHANGED,
                    value: file1Arr[matchIndex1]
                });
            }

            matchPreviousIndex1 = ++matchIndex1;
            matchPreviousIndexStart = ++matchIndex2;
        }
    }

    return result;
}

module.exports = {
    compareFiles: compareFiles
};