'use strict';

const lineInfo = require('./lineInfo');
const STATUSES = lineInfo.statuses;

/**
 * @param {Array} file1Arr
 * @param {Array} file2Arr
 * @param {Array} result
 * @returns {Array}
 */
function compareFiles(file1Arr, file2Arr, result = []) {
    let matchIndex1 = null;
    let matchIndex2 = null;

    for (let i = 0; i < file1Arr.length; i++) {
        for (let j = 0; j < file2Arr.length; j++) {
            if (file1Arr[i] === file2Arr[j]) {
                matchIndex1 = i;
                matchIndex2 = j;
                break;
            }
        }

        if (matchIndex1 !== null || matchIndex2 !== null) {
            break;
        }
    }

    const matchNotFound = matchIndex1 === null || matchIndex2 === null;

    let linesBeforeMatch1 = matchNotFound ? file1Arr : file1Arr.slice(0, matchIndex1);
    let linesBeforeMatch2 = matchNotFound ? file2Arr : file2Arr.slice(0, matchIndex2);

    const maxLength = Math.max(linesBeforeMatch1.length, linesBeforeMatch2.length);

    for (let i = 0; i < maxLength; i++) {
        result.push(lineInfo.getLineInfo(linesBeforeMatch1[i], linesBeforeMatch2[i]));
    }

    if (!matchNotFound) {
        result.push({
            status: STATUSES.STATUS_NOT_CHANGED,
            value: file1Arr[matchIndex1]
        });
    }

    if (matchNotFound) {
        return result;
    }

    return compareFiles(file1Arr.slice(matchIndex1+1), file2Arr.slice(matchIndex2+1), result);
}

module.exports = {
    compareFiles: compareFiles
};