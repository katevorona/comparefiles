'use strict';

const INDEX_OF_FILEPATH_ARGUMENTS = 2; // first element it's node, second it's filename
/**
 * @param {String} file1
 * @param {String} file2
 * @param {Object[]} result
 * @param {String} result.status
 * @param {String} result.value
 */
function outputResult(file1, file2, result) {
    console.log(`Comparing "${file1}" with "${file2}":`);
    result.forEach((line, i) => {
        console.log(`${i+1}   ${line.status}    ${line.value}`);
    });
    console.log(`------------------------------------\n`);
}

/**
 * @return {String[]}
 */
function getFileNames() {
    let filesPaths = [];
    for(let i = INDEX_OF_FILEPATH_ARGUMENTS; i < process.argv.length; i++){
        filesPaths.push(process.argv[i]);
    }
    return filesPaths;
}

module.exports = {
    outputResult: outputResult,
    getFileNames: getFileNames
};
