'use strict';

const cycleWay = require('./src/compareFilesCycle.js');
const readingFiles = require('./src/readingFiles.js');
const inputOutput = require('./src/inputOutput.js');

const filesArr = readingFiles.getArrFromFiles(inputOutput.getFileNames());

if(filesArr.length < 2) {
    console.log("We need at least two files for comparing :( ");
} else {
    for (let i = 0; i < filesArr.length - 1; i++) {
        let result = cycleWay.compareFiles(filesArr[i].fileArr, filesArr[i + 1].fileArr);
        inputOutput.outputResult(filesArr[i].filename, filesArr[i + 1].filename, result);
    }
}